/**
 * Constants to avoid being prone to misstypes
 */
module.exports = {
  X: "X",
  O: "O"
};
