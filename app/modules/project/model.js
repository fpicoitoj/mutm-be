const mongoose = require("mongoose");

class ProjectModel {
  constructor() {
    this.schema = this.schema();
  }

  schema() {
    const taskSchema = new mongoose.Schema(
      {
        description: { type: String, required: true },
        finishedAt: { type: Date, default: null }
      },
      { timestamps: true }
    );

    return new mongoose.Schema(
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
        name: { type: String, required: true },
        tasks: [taskSchema]
      },
      { timestamps: true }
    );
  }
}

module.exports = mongoose.model("Project", new ProjectModel().schema);
