const express = require("express");
const projectController = require("./controller");
const authMiddleware = require("./../auth/middleware");
const task = require("./../task/index");

class Router {
  constructor() {
    this.router = express.Router();
    this.init();
  }

  /**
   * Get all projects
   *
   * @param {*} req
   * @param {*} res
   */
  async getAll(req, res) {
    try {
      const projects = await projectController.getAll(req.user);
      return res.status(200).json({ projects });
    } catch (err) {
      return res
        .status(500)
        .json({ code: "PROJECT.GET-ALL", message: err.message });
    }
  }

  /**
   * Get project
   *
   * @param {*} req
   * @param {*} res
   */
  async get(req, res) {
    try {
      const project = await projectController.get(req.params.id);
      return res.status(200).json(project);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "PROJECT.GET", message: err.message });
    }
  }

  /**
   * Create a project
   *
   * @param {*} req
   * @param {*} res
   */
  async create(req, res) {
    try {
      const project = await projectController.create(req.user, req.body);
      return res.status(200).json(project);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "PROJECT.CREATE", message: err.message });
    }
  }

  /**
   * Update a project
   *
   * @param {*} req
   * @param {*} res
   */
  async update(req, res) {
    try {
      const project = await projectController.update(req.params.id, req.body);
      return res.status(200).json(project);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "PROJECT.UPDATE", message: err.message });
    }
  }

  /**
   * Delete a project
   *
   * @param {*} req
   * @param {*} res
   */
  async delete(req, res) {
    try {
      const project = await projectController.delete(req.params.id);
      return res.status(200).json(project);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "PROJECT.DELETE", message: err.message });
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.param("project", async (req, res, next, projId) => {
      req.project = await projectController.get(req.params.project);

      next();
    });

    this.router.get("/all", authMiddleware.blockGuest, this.getAll);
    this.router.get("/:id", authMiddleware.blockGuest, this.get);
    this.router.post("/", authMiddleware.blockGuest, this.create);
    this.router.put("/:id", authMiddleware.blockGuest, this.update);
    this.router.delete("/:id", authMiddleware.blockGuest, this.delete);
    this.router.use("/:project/tasks", authMiddleware.blockGuest, task);
  }
}

module.exports = new Router().router;
