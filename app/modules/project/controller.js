const ProjectModel = require("./model");

class Controller {
  /**
   * Get all projects and it's tasks
   *
   * @param {ObjectId} id
   */
  async getAll(user) {
    return await ProjectModel.find({ user: user._id }).populate(
      "tasks.location"
    );
  }

  /**
   * Get a project
   *
   * @param {ObjectId} id
   */
  async get(id) {
    const project = await ProjectModel.findById(id);

    if (!project) throw Error("Project not found");

    return project;
  }

  /**
   * Create a new project
   */
  async create(user, { name }) {
    return await new ProjectModel({ user: user._id, name }).save();
  }

  /**
   * Create a new project
   *
   * @param {ObjectId} id
   */
  async update(id, { name }) {
    const project = await this.get(id);

    project.set({ name });

    return project.save();
  }

  /**
   * Create a new project
   *
   * @param {ObjectId} id
   */
  async delete(id) {
    const project = await this.get(id);

    return await project.remove();
  }
}

module.exports = new Controller();
