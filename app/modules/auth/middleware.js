const jsonwebtoken = require("jsonwebtoken");
const UserModel = require("./../user/model");
const UserController = require("./../user/controller");

class AuthMiddleware {
  async getUserFromJWT(req, res, next) {
    if (!req.headers || !req.headers.authorization) return next();

    // retrieve token from auth "Bearer AUTHSTRINGHERE"
    const [_, authorization] = req.headers.authorization.split(" ");

    try {
      const decoded = jsonwebtoken.verify(
        authorization,
        process.env.JWT_SECRET
      );

      if (!decoded) next();

      const userId = decoded._id;
      const user = await UserController.get(userId);

      if (user) req.user = user;

      next();
    } catch (e) {
      next();
    }
  }

  async blockGuest(req, res, next) {
    if (typeof req.user === "undefined") return next(Error("unauthorized"));

    next();
  }
}

const middleware = new AuthMiddleware();
module.exports = middleware;
