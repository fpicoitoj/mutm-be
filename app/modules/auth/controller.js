const userController = require("./../user/controller");
const jwt = require("jsonwebtoken");

class Controller {
  async register({ name, email, password }) {
    const user = await userController.getByEmail(email);

    if (user) throw Error("User is already registered");

    return await userController.create({ name, email, password });
  }

  async login({ email, password }) {
    const user = await userController.getByEmail(email);

    if (!user || !user.comparePassword(password))
      throw Error("Credentials do not match");

    return {
      user,
      token: `${process.env.JWT_KEY} ${jwt.sign(
        { _id: user._id, email: user.email },
        process.env.JWT_SECRET,
        { expiresIn: "60m" }
      )}`
    };
  }

  verify(user) {
    if (!user) throw Error("JWT invalid");

    return user;
  }
}

module.exports = new Controller();
