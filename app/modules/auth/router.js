const express = require("express");
const authController = require("./controller");
const middleware = require("./middleware");

class Router {
  constructor() {
    this.router = express.Router();
    this.init();
  }

  /**
   * Register
   *
   * @param {*} req
   * @param {*} res
   */
  async register(req, res) {
    try {
      const registration = await authController.register(req.body);
      return res.status(200).json(registration);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "AUTH.REGISTER", message: err.message });
    }
  }

  /**
   * Login
   *
   * @param {*} req
   * @param {*} res
   */
  async login(req, res) {
    try {
      const loggedIn = await authController.login(req.body);
      return res.status(200).json(loggedIn);
    } catch (err) {
      return res.status(500).json({ code: "AUTH.LOGIN", message: err.message });
    }
  }

  /**
   * Verifies JWT
   *
   * @param {*} req
   * @param {*} res
   */
  async verify(req, res) {
    try {
      const verify = await authController.verify(req.user);
      return res.status(200).json(verify);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "AUTH.VERIFY", message: err.message });
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.post("/register", this.register);
    this.router.post("/login", this.login);
    this.router.post("/verify", middleware.getUserFromJWT, this.verify);
  }
}

module.exports = new Router().router;
