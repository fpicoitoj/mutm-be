const express = require("express");
const userController = require("./controller");
const authMiddleware = require("./../auth/middleware");

class Router {
  constructor() {
    this.router = express.Router();
    this.init();
  }

  /**
   * Get all users
   *
   * @param {*} req
   * @param {*} res
   */
  async getAll(req, res) {
    try {
      const users = await userController.getAll();
      return res.status(200).json(users);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "USER.GET-ALL", message: err.message });
    }
  }

  /**
   * Get user
   *
   * @param {*} req
   * @param {*} res
   */
  async get(req, res) {
    try {
      const user = await userController.get(req.params.id);
      return res.status(200).json(user);
    } catch (err) {
      return res.status(500).json({ code: "USER.GET", message: err.message });
    }
  }

  /**
   * Create a user
   *
   * @param {*} req
   * @param {*} res
   */
  async create(req, res) {
    try {
      const user = await userController.create(req.body);
      return res.status(200).json(user);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "USER.CREATE", message: err.message });
    }
  }

  /**
   * Delete a user
   *
   * @param {*} req
   * @param {*} res
   */
  async delete(req, res) {
    try {
      const user = await userController.delete(req.params.id);
      return res.status(200).json(user);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "USER.DELETE", message: err.message });
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.get("/all", authMiddleware.blockGuest, this.getAll);
    this.router.get("/:id", this.get);
    this.router.post("/", this.create);
    this.router.delete("/:id", this.delete);
  }
}

module.exports = new Router().router;
