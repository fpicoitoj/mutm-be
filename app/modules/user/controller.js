const UserModel = require("./model");

class Controller {
  /**
   * Get all users
   *
   * @param {ObjectId} id
   */
  async getAll() {
    return await UserModel.find();
  }

  /**
   * Get a user
   *
   * @param {ObjectId} id
   */
  async get(id) {
    const user = await UserModel.findById(id);

    if (!user) throw Error("User not found");

    return user;
  }

  /**
   * Get a user by email
   *
   * @param {ObjectId} id
   */
  async getByEmail(email) {
    return await UserModel.findOne({ email }).exec();
  }

  /**
   * Create a new user
   */
  async create({ name, email, password }) {
    return await new UserModel({ name, email, password }).save();
  }

  /**
   * Create a new user
   *
   * @param {ObjectId} id
   */
  async delete(id) {
    const user = await this.get(id);

    return await user.remove();
  }
}

module.exports = new Controller();
