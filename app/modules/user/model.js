const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const saltRounds = 10;

class UserModel {
  constructor() {
    this.schema = this.schema();
    this.triggers();
    this.methods();
  }

  schema() {
    return new mongoose.Schema(
      {
        name: { type: String, required: true },
        email: { type: String, trim: true, required: true },
        password: { type: String, required: true }
      },
      { timestamps: true }
    );
  }

  triggers() {
    // before saving
    this.schema.pre("save", function(next) {
      // always hash password
      if (this.isModified("password")) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
      }

      next();
    });
  }

  methods() {
    // Compare the given :password with the user's password field
    this.schema.methods.comparePassword = function(password) {
      return bcrypt.compareSync(password, this.password);
    };

    this.schema.methods.hashPassword = function(password) {
      return bcrypt.hashSync(password, saltRounds);
    };
  }
}

module.exports = mongoose.model("User", new UserModel().schema);
