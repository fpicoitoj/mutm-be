const UserModel = require("./../user/model");
const UserController = require("./../user/controller");

class TaskMiddleware {
  async fetchProject(req, res, next) {
    if (typeof req.user === "undefined") return next(Error("unauthorized"));

    console.log(req.params);

    next();
  }
}

const middleware = new TaskMiddleware();
module.exports = middleware;
