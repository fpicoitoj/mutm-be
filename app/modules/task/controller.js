const ProjectModel = require("./../project/model");

class Controller {
  /**
   * Get all tasks
   *
   * @param {ObjectId} id
   */
  getAll(project) {
    return project.tasks;
  }

  /**
   * Get a task
   *
   * @param {ObjectId} id
   */
  async get(project, id) {
    const task = await project.tasks.id(id);

    if (!task) throw Error("Task not found");

    return task;
  }

  /**
   * Create a new task
   */
  async create(project, { description }) {
    const newTask = project.tasks.create({ description });

    project.tasks.push(newTask);

    const newProject = await project.save();

    return newTask;
  }

  /**
   * Update a task
   *
   * @param {ObjectId} id
   */
  async update(project, id, { description }) {
    const task = await this.get(project, id);

    if (task.finishedAt) throw Error("Task cannot be modified");

    task.description = description;

    await project.save();

    return task;
  }

  /**
   * Complete a task
   */
  async complete(project, id) {
    const task = await this.get(project, id);

    task.finishedAt = new Date();

    await project.save();

    return task;
  }

  /**
   * Uncomplete a task
   */
  async uncomplete(project, id) {
    const task = await this.get(project, id);

    task.finishedAt = null;

    await project.save();

    return task;
  }

  /**
   * Delete a task
   *
   * @param {ObjectId} id
   */
  async delete(project, id) {
    const task = await this.get(project, id);

    if (task.finishedAt) throw Error("Task cannot be deleted");

    await task.remove();
    await project.save();

    return task;
  }
}

module.exports = new Controller();
