const Router = require("./router");

class Main {
  constructor() {
    this.router = Router;
  }
}

module.exports = new Main().router;
