const express = require("express");
const taskController = require("./controller");
const authMiddleware = require("./../auth/middleware");

class Router {
  constructor() {
    this.router = express.Router();
    this.init();
  }

  /**
   * Get all tasks
   *
   * @param {*} req
   * @param {*} res
   */
  async getAll(req, res) {
    try {
      const tasks = await taskController.getAll(req.project, req.user);
      return res.status(200).json(tasks);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "TASK.GET-ALL", message: err.message });
    }
  }

  /**
   * Get task
   *
   * @param {*} req
   * @param {*} res
   */
  async get(req, res) {
    try {
      const task = await taskController.get(req.project, req.params.id);
      return res.status(200).json(task);
    } catch (err) {
      return res.status(500).json({ code: "TASK.GET", message: err.message });
    }
  }

  /**
   * Create a task
   *
   * @param {*} req
   * @param {*} res
   */
  async create(req, res) {
    try {
      const task = await taskController.create(req.project, req.body);
      return res.status(200).json(task);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "TASK.CREATE", message: err.message });
    }
  }

  /**
   * Update a task
   *
   * @param {*} req
   * @param {*} res
   */
  async update(req, res) {
    try {
      const task = await taskController.update(
        req.project,
        req.params.id,
        req.body
      );
      return res.status(200).json(task);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "PROJECT.UPDATE", message: err.message });
    }
  }

  /**
   * Delete a task
   *
   * @param {*} req
   * @param {*} res
   */
  async delete(req, res) {
    try {
      const task = await taskController.delete(req.project, req.params.id);
      return res.status(200).json(task);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "TASK.DELETE", message: err.message });
    }
  }

  /**
   * Complete a task
   *
   * @param {*} req
   * @param {*} res
   */
  async complete(req, res) {
    try {
      const task = await taskController.complete(req.project, req.params.id);
      return res.status(200).json(task);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "TASK.COMPLETE", message: err.message });
    }
  }

  /**
   * Uncomplete a task
   *
   * @param {*} req
   * @param {*} res
   */
  async uncomplete(req, res) {
    try {
      const task = await taskController.uncomplete(req.project, req.params.id);
      return res.status(200).json(task);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "TASK.UNCOMPLETE", message: err.message });
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.get("/all", authMiddleware.blockGuest, this.getAll);
    this.router.get("/:id", authMiddleware.blockGuest, this.get);
    this.router.post("/:id/complete", authMiddleware.blockGuest, this.complete);
    this.router.post(
      "/:id/uncomplete",
      authMiddleware.blockGuest,
      this.uncomplete
    );
    this.router.post("/", authMiddleware.blockGuest, this.create);
    this.router.put("/:id", authMiddleware.blockGuest, this.update);
    this.router.delete("/:id", authMiddleware.blockGuest, this.delete);
  }
}

module.exports = new Router().router;
