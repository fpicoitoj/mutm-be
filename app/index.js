const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");

// Modules
const auth = require("./modules/auth");
const user = require("./modules/user");
const project = require("./modules/project");

// Middleware
const userMiddleware = require("./modules/auth/middleware");

/**
 * Organize app bootup with class for better readability
 */
class App {
  constructor() {
    this.express = express();
    this.middleware();
    this.database();
    this.routes();
  }

  middleware() {
    // Given the complexity of some configuration we ought to
    // place them in a separate file
    this.express.use(
      cors({
        origin: process.env.CORS_ORIGIN,
        optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
      })
    );
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(userMiddleware.getUserFromJWT); // user is now carried in all requests
  }

  database() {
    mongoose.Promise = global.Promise; // use built-in Promise
    mongoose.connect(process.env.DATABASE_URL);
  }

  routes() {
    const apiPathV1 = "/api/v1";

    // Hello world for API instead of ugly Cannot GET /
    this.express.get("/", (req, res) => {
      res.send("Hello from MUTM!");
    });

    this.express.use(`${apiPathV1}/auth`, auth);
    this.express.use(`${apiPathV1}/users`, user);
    this.express.use(`${apiPathV1}/projects`, project);

    this.express.use(function(err, req, res, next) {
      // technical debt - fix if enough time #todo
      const status = err.message === "unauthorized" ? 401 : 500;

      res.status(status).send({
        message: err.message
      });
    });
  }
}

module.exports = new App().express;
